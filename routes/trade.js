const express = require('express');
const asyncHandler = require('express-async-handler');
const tradeController = require('../controllers/trade');
const { populate } = require('../models/trade');

const router = new express.Router();

const createTrade = asyncHandler(async (req, res, next) => {
  let trade = await tradeController.createTrade(req.body);
  return res.send(trade)
})

const findTrades = asyncHandler(async (req, res, next) => {
  let { tradeG1, tradeG2 } = req.body;
  let searchObject = {
    'giving.category': tradeG2._id,
    'receiving.category': tradeG1._id,
  };
  let populate = [{ path: 'owner' }, { path: 'giving.category' }, { path: 'receiving.category' }]
  let trade = await tradeController.findTrades(searchObject, null, null, populate)
  return res.send(trade)
})

router.post('/registerTrade', createTrade)
router.post('/searchTrades', findTrades)

module.exports = router