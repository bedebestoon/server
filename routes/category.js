const express = require('express');
const asyncHandler = require('express-async-handler');
const categoryController = require('../controllers/category');

const router = new express.Router();

const createCategory = asyncHandler(async(req, res, next) => {
  let category = await categoryController.createCategory(req.body);
  return res.send(category)
})

const getAllCategories = asyncHandler(async(req, res, next)=>{
  let category = await categoryController.findCategories()
  return res.send(category)
})

router.post('/createNewCategory', createCategory)
router.get('/getAllCategories', getAllCategories)

module.exports = router