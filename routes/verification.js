const express = require('express');
const router = new express.Router();
const asyncWrapper = require('express-async-handler');

const verificationController = require('../controllers/verification');
const userController = require('../controllers/user')

const authMiddleware = require('../middlewares/auth');

let createVerificationCode = asyncWrapper(async (req, res, next) => {
  try {
    let user = await userController.findOneUserSafe({
      phoneNumber: req.body.phoneNumber
    })
    if (!body.resetPass)
      res.send(user._id)
    else
      handleCode()
  } catch {
    handleCode()
  }
  async function handleCode() {
    let code = await verificationController.createVerificationCode(req.body.phoneNumber)
    if (code) {
      console.log(`${req.body.phoneNumber} Verification Code is :`, code.code)
      res.send('OK')
    }
  }
})

let verifyCode = asyncWrapper(async (req, res, next) => {
  let verify = await verificationController.findOneVerificationToken({
    code: req.body.code,
    phone: req.body.phoneNumber
  })
  if (verify.phone) {
    res.send('OK')
  }
})

router.post('/getVerificationCode',
  authMiddleware.authenticateUser(),
  createVerificationCode
)

router.post('/verifyCode',
  authMiddleware.authenticateUser(),
  verifyCode
)

module.exports = router