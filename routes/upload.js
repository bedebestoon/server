const express = require('express');
const router = new express.Router();
const asyncWrapper = require('express-async-handler');

const uploadHelper = require('../utils/upload').fileUploadHelper;
const authenticateUser = require('../middlewares/auth').authenticateUser
const roles = require('../consts').USER.ROLES

const config = require('../config.json');

const image = asyncWrapper(async (req, res, next) => {
  let { user } = req.locals || {}
  let folderId = req.body && req.body.id ? req.body.id : null
  let fileAddress = await uploadHelper(
    req.files,
    config.uploadFormName,
    config.imageUploadExt,
    config.folders.static + config.folders.pic + "/" + (user ? `${user._id}` : ""),
    folderId
  )
  res.send(config.folders.prefix + config.folders.URIaddress + config.folders.pic + (user ? `/${user._id}` : "") + "/" + fileAddress)
})

router.post(
  '/image',
  // authenticateUser([roles.ADMIN, roles.CUSTOMER]),
  image
)

module.exports = router