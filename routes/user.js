const express = require('express');
const router = new express.Router();
const asyncWrapper = require('express-async-handler');

const userController = require('../controllers/user');

const authMiddleware = require('../middlewares/auth');

const password = require('../utils/password');
const auth = require('../utils/auth');

const registerUser = asyncWrapper(async (req, res, next) => {
  const hash = await password(req.body.password).hash();
  delete req.body.password;
  const user = await userController.createUserSafe({
    ...req.body,
    passwordHash: hash,
  });
  return res.send(user);
})

const loginUser = asyncWrapper(async (req, res, next) => {
  let user;
  try {
    user = await userController.findOneUser({
      phoneNumber: req.body.username.toLowerCase(),
    });
  } catch (e) {
    user = await userController.findOneUser({
      mail: req.body.username,
    });
  }
  await password(req.body.password).verify(user.passwordHash);
  let tokenObjectModel = {
    _id: user._id,
  }
  let token = await auth.tokenModelGenerator(tokenObjectModel)
  return res.send({ token, info: user });
})

const getUserByPhoneNumber = asyncWrapper(async (req, res, next) => {
  const user = await userController.findOneUserSafe({
    phoneNumber: req.body.phoneNumber
  })
  return res.send(user._id)
})

router.post(
  '/register',
  authMiddleware.authenticateUser(),
  registerUser,
);
router.post(
  '/login',
  authMiddleware.authenticateUser(),
  loginUser
);
router.post(
  '/getUserByPhoneNumber',
  getUserByPhoneNumber
)

module.exports = router