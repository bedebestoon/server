const asyncWrapper = require('express-async-handler');

const userController = require('../controllers/user');
const roles = require('../consts').USER.ROLES

const jwt = require('../utils/jwt');
const auth = require('../utils/auth');

const userStatuses = require("../consts").USER.STATUSES
let authenticateUser = (userRoles) =>
  asyncWrapper(async (req, res, next) => {
    let userRolesPrivate = userRoles;
    if (typeof userRoles == 'string') userRolesPrivate = [userRolesPrivate];
    userRolesPrivate = userRolesPrivate || [];
    req.locals = {
      ...req.locals,
      authenticated: false,
    };
    let token = req.headers['x-access-token'];
    if (
      (token == undefined || token == 'null') &&
      userRolesPrivate.length == 0
    ) {
      return next();
    }

    let authenticated = false;
    let tokenObject = await jwt.decode(token);
    await auth.tokenIsNotExpired(tokenObject);
    delete tokenObject.expires;
    userController.findOneUser(tokenObject)
      .then((user) => {
        console.log('A REQUEST IS PERFORMING BY ==> ', 'role: ', user.role.name, 'id: ', user._id)
        if (user.status !== userStatuses.ACTIVE) {
          return res.status(401).send('user has been suspended');
        }
        if (userRolesPrivate.length == 0) {
          authenticated = true;
        }

        let userId = user._id

        userRolesPrivate.forEach(role => {
          if (role === user.role.name) authenticated = true
          if (role === roles.OWNER) {
            console.log(req.params.id, req.body._id)
            if ((req.params.id && req.params.id === String(userId)) || (req.body._id && req.body._id === String(userId)))
              authenticated = true
          }
        })

        if (!authenticated) {
          return res.status(401).send('Not Authenticated');
        }
        req.locals = {
          ...req.locals,
          authenticated: true,
          user: user,
        };
        next();
      })
      .catch((err) => res.status(401).send({ err }))
  });

const authPasswordHashedToken = asyncWrapper(async (req, res, next) => {
  req.locals = {
    ...req.locals,
    authenticated: false,
  };
  let user = await userController.findOneUser({ username: req.body.username });
  const token = req.headers['x-access-token'];
  let tokenObject = await jwt.decode(token, user.password);
  await auth.tokenIsNotExpired(tokenObject);
  delete tokenObject.expires;
  user = await userController.findOneUser(tokenObject);
  req.locals = {
    ...req.locals,
    authenticated: true,
    user: user,
  };
  next();
});

module.exports = {
  authenticateUser,
  authPasswordHashedToken,
};
