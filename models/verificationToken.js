const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('../config.json');
const consts = require('../consts').VERIFICATION_TYPES;

const verificationTokenSchema = new Schema({
  phone: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now,
    expires: config.verificationCodeValidityTimeMinutes + 'm',
  },
});
const verificationTokenModel = mongoose.model(
  'verificationToken',
  verificationTokenSchema,
);

module.exports = verificationTokenModel;
