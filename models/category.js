const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const consts = require('../consts').CATEGORY;

const categorySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  normalizedName: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: consts.TYPES_ARR,
    default: consts.TYPES.MERCHANDISE,
    required: true
  },
});

const categoryModel = mongoose.model('category', categorySchema);

module.exports = categoryModel;
