const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const consts = require('../consts').TRADE;

const tradeSchema = new Schema({
  owner: {
    type: Schema.Types.ObjectId,
    ref: 'user',
    required: true,
  },
  type: {
    type: String,
    enum: consts.TYPES_ARR,
    default: consts.TYPES.M2M,
    required: true,
  },
  status: {
    type: String,
    enum: consts.STATUSES_ARR,
    default: consts.STATUSES.ACTIVE,
    required: true,
  },
  images: {
    type: Array,
    default: [],
    required:  true,
  },
  giving: {
    category: {
      type: Schema.Types.ObjectId,
      ref: 'category',
    },
    fullName: {
      type: String
    },
    desc: {
      type: String
    }
  },
  receiving: {
    category: {
      type: Schema.Types.ObjectId,
      ref: 'category',
    },
    fullName: {
      type: String,
    },
    desc: {
      type: String,
    }
  },
});

const tradeModel = mongoose.model('trade', tradeSchema);

module.exports = tradeModel;