const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const consts = require('../consts').USER;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  phoneNumber: {
    type: String,
    required: true,
    unique: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
  },
  role: {
    type: String,
    enum: consts.ROLES_ARR,
    default: consts.ROLES.CUSTOMER,
    required: true,
  },
  status: {
    type: String,
    enum: consts.STATUSES_ARR,
    default: consts.STATUSES.ACTIVE,
    required: true,
  },
  place: {
    province: {
      type: String
    },
    city: {
      type: String
    }
  },
  trades: [
    {
      type: Schema.Types.ObjectId,
      ref: 'trade',
    }
  ]
});

const userModel = mongoose.model('user', userSchema);

module.exports = userModel;
