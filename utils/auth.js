const jwt = require('./jwt');

const config = require('../config.json');
// const permissions = require('../permissions');

const genTokenExpireDateFromNow = (days) => {
  let date = new Date();
  date.setDate(date.getDate() + (days || config.tokenValidityTimeDays));
  return date;
};

const genTokenExpireMinutesFromNow = (mins) => {
  let date = new Date();
  date.setMinutes(date.getMinutes() + mins);
  return date;
};

const tokenIsNotExpired = (tokenObject) =>
  new Promise((resolve, reject) => {
    let exp = new Date(tokenObject.expires);
    if (exp.getTime() > Date.now()) {
      return resolve(true);
    }
    let err = new Error('token has Expired');
    err.status = 403;
    reject(err);
  })

const userHasPermissions = (perms, user, model) =>
  new Promise((resolve, reject) => {
    let permsPrivate = perms;
    let modelPrivate = model;
    if (permsPrivate == undefined)
      return reject('no permissions has been set');
    if (typeof permsPrivate == 'string') permsPrivate = [permsPrivate];
    let err = new Error('no user is set');
    err.status = 403;
    if (user == undefined) {
      if (permsPrivate.findIndex((val) => val === permissions.GUEST) !== -1) {
        return resolve(true);
      }
      return reject(err);
    }
    modelPrivate = modelPrivate || {};
    modelPrivate.owner = modelPrivate.owner || {};
    if (
      permsPrivate.findIndex((val) => val === permissions.OWNER) !== -1 &&
      (modelPrivate.username || modelPrivate.owner.username)
    ) {
      if (
        user.username == modelPrivate.username ||
        user.username == modelPrivate.owner.username
      ) {
        return resolve(true);
      }
    }
    for (let perm of permsPrivate) {
      if (user.role === perm) return resolve(true);
    }
    reject(notAuthorized(user.username));
  })

const tokenModelGenerator = async (user, expiresInDays) => {
  let token = await jwt.encode({
    _id: user._id,
    expires: genTokenExpireDateFromNow(expiresInDays),
  })
  return token
}

module.exports = {
  tokenModelGenerator,
  genTokenExpireDateFromNow,
  genTokenExpireMinutesFromNow,
  tokenIsNotExpired,
  // userHasPermissions
}
