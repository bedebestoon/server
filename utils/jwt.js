const jwt = require('jwt-simple');
const config = require('../config.json');

const encode = (payload, secret) =>
    new Promise((res, rej) => {
      let token;
      try {
        token = jwt.encode(
          payload,
          secret || config.jwtSecret,
          config.hashAlgo,
        );
      } catch (e) {
        return rej('couldnt generate token');
      }
      if (!token) return rej('couldnt generate token');
      res(token);
    })

const decode = (token, secret) =>
    new Promise((res, rej) => {
      let payload;
      let err = new Error('token is not valid');
      err.status = 403;
      try {
        payload = jwt.decode(
          token,
          secret || config.jwtSecret,
          false,
          config.hashAlgo,
        );
      } catch (e) {
        return rej(err);
      }
      if (!payload) return rej(err);
      res(payload);
    })

module.exports = {
  encode,
  decode
}
