const pass = require('password-hash-and-salt');

const password = (password) => {
  let p = password;
  return {
    hash: () =>
      new Promise((res, rej) => {
        pass(p).hash((err, hash) => {
          if (err) return rej(err);
          return res(hash);
        });
      }),

    verify: (hash) =>
      new Promise((res, rej) => {
        pass(p).verifyAgainst(hash, (err, ver) => {
          if (err) return rej(err);
          if (!ver) {
            err = new Error('password not correct');
            err.status = 403;
            return rej(err);
          }
          res(true);
        });
      }),
  };
};

module.exports = password;
