const uuidv4 = require('uuid/v4');

let fileUploadHelper = (files, fileName, exts, uploadDirectory) =>
  new Promise((resolve, reject) => {
    if (!files) {
      return reject('no files was presented');
    }
    if (RegExp(exts).test(files[fileName].name.toLowerCase())) {
      let filename = `${uuidv4()}${
        RegExp(exts).exec(files[fileName].name.toLowerCase())[0]
      }`;
      files[fileName].mv(`${uploadDirectory}${filename}`, (err) => {
        if (err) {
          return reject(err);
        }

        return resolve(filename);
      });
    } else {
      return reject('not a valid file');
    }
  });

module.exports = {
  fileUploadHelper,
};
