const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const helmet = require('helmet');
const mongoose = require('mongoose');
const cors = require('cors')

const fileUpload = require('express-fileupload');

const config = require('./config.json');

const dbConfig = config.localDB
const port = process.env.PORT || config.port || 3000;

const app = express()


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(helmet());
app.use(cors())

app.use(
  fileUpload({
    limits: {fileSize: config.fileUploadLimit},
    // safeFileNames: true,
    preserveExtension: true,
    abortOnLimit: true,
  }),
);

app.use(express.static(config.folders.static))

app.use('/', require('./router'));

app.use(function(err, req, res, next) {
  console.log('error', err);
  if (err.messageObj) {
    res.status(err.status).send(err.messageObj);
  } else if (err.status) {
    res.status(err.status).send(err.message);
  } else {
    res.status(500).send(err.message || err);
  }
});

mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

mongoose.connect(dbConfig);

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'db connection error:'));
db.once('open', function() {
  console.log('database connection successful with ' + dbConfig);
});

app.listen(port, function(err) {
  if (err) throw err;
  console.log('BedeBestoon RESTful api listening on port ' + config.port);
});
