const express = require('express');
const router = new express.Router()
// subRoutes
const user = require('./routes/user')
const upload = require('./routes/upload')
const trade = require('./routes/trade')
const category = require('./routes/category')
const verification = require('./routes/verification')

// MainRoutes
router.use('/user', user)
router.use('/upload', upload)
router.use('/trade', trade)
router.use('/category', category)
router.use('/verification', verification)

module.exports = router