const verificationCodeModel = require('../models/verificationToken');

var randomize = require('randomatic');

const config = require('../config.json');

let createVerificationCode = (phone) =>
  new Promise((resolve, reject) => {
    let q = {phone};
    verificationCodeModel.findOne(q, (err, res) => {
      if (err) return reject(err);
      if (res != undefined) {
        return resolve(res);
      }
      const code = randomize(
        config.verificationCodePattern,
        config.verificationCodeLength,
      );
      verificationCodeModel
        .create({phone, code})
        .then((val) => resolve(val))
        .catch((er) => reject(er));
    });
  });
  
let findOneVerificationToken = (condition, projection, option) =>
  new Promise((resolve, reject) =>
    verificationCodeModel.findOne(
      condition,
      projection,
      option,
      (err, res) => {
        if (err) return reject(err);
        if (res == undefined) {
          return reject('no verificationToken has been found');
        }
        resolve(res);
      },
    ),
  );

const verificationController = {
  createVerificationCode,
  findOneVerificationToken,
};

module.exports = verificationController;
