const categoryModel = require('../models/category');

let createCategory = (category) => categoryModel.create(category);

let findCategories = (condition, projection, option) =>
  new Promise((resolve, reject) =>
  categoryModel.find(condition, projection, option, (err,res) => {
    if (err) return reject(err);
    resolve(res)
  }),
);

let updateCategory = (condition, update, option) =>
  new Promise((resolve, reject) =>
    categoryModel.findOneAndUpdate(
      condition,
      update,
      {...option, runValidators: true},
      (err, doc) => {
        if (err) return reject(err)
        let resp = {
          ...doc._doc,
          ...update,
        };
        resolve(resp)
      },
    ),
  );

const categoryController = {
  createCategory,
  findCategories,
  updateCategory,
};

module.exports = categoryController