const userModel = require('../models/user');

let createUser = (user) => userModel.create(user);
let createUserSafe = (user) =>
  new Promise((resolve, reject) => {
    createUser(user)
      .then((result) => {
        delete result.password;
        resolve(result);
      })
      .catch((e) => {
        reject(e);
      });
  });

let findUsers = (condition, projection, option) =>
  new Promise((resolve, reject) =>
    userModel.find(condition, projection, option, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    }),
  );
  
let findOneUser = (condition, projection, option) =>
  new Promise((resolve, reject) =>
    userModel.findOne(condition, projection, option, (err, res) => {
      if (err) return reject(err);
      if (res == undefined) return reject('no user has been found');
      resolve(res);
    }),
  );

let findUsersSafe = (condition, projection, option) =>
  findUsers(condition, { ...projection, password: 0 }, option);
let findOneUserSafe = (condition, projection, option) =>
  findOneUser(condition, { ...projection, password: 0 }, option);

let updateUser = (condition, update, option) =>
  new Promise((resolve, reject) =>
    userModel.findOneAndUpdate(
      condition,
      update,
      { ...option, runValidators: true },
      (err, user) => {
        if (err) return reject(err);
        let resp = {
          ...user._doc,
          ...update,
        };
        resolve(resp);
      },
    ),
  );

let updateUserSafe = (condition, update, option) =>
  new Promise((resolve, reject) => {
    updateUser(condition, update, option)
      .then((user) => {
        delete user.password;
        resolve(user);
      })
      .catch((e) => {
        reject(e);
      });
  });

const userController = {
  createUser,
  findUsersSafe,
  createUserSafe,
  findOneUser,
  findOneUserSafe,
  updateUserSafe,
};

module.exports = userController;
