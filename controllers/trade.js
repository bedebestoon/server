const tradeModel = require('../models/trade');

let createTrade = (Trade) => tradeModel.create(Trade);

let findTrades = (condition, projection, option, populate) =>
  new Promise((resolve, reject) =>
    tradeModel.find(condition, projection, option)
      .populate(populate)
      .exec((err, res) => {
        if (err) return reject(err);
        resolve(res)
      })
  )

let updateTrade = (condition, update, option) =>
  new Promise((resolve, reject) =>
    tradeModel.findOneAndUpdate(
      condition,
      update,
      { ...option, runValidators: true },
      (err, doc) => {
        if (err) return reject(err)
        let resp = {
          ...doc._doc,
          ...update,
        };
        resolve(resp)
      },
    ),
  );

const TradeController = {
  createTrade,
  findTrades,
  updateTrade,
};

module.exports = TradeController